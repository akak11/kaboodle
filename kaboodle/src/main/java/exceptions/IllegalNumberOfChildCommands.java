package exceptions;

public class IllegalNumberOfChildCommands extends KaboodleException {

    public IllegalNumberOfChildCommands(String commandName, int numOfChildCommands) {
        super(
                "Illegal number of child commands ("
                        + numOfChildCommands
                        + ") for command '"
                        + commandName
                        + "'");
    }
}
