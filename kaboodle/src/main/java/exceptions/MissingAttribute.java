package exceptions;

public class MissingAttribute extends AttributesException {

    public MissingAttribute(String commandName) {
        super("Missing must have property for command '" + commandName + "'");
    }
}
