package exceptions;

public class NoSuchAttributeForCommand extends AttributesException {

    public NoSuchAttributeForCommand(String commandName, String propertyName) {
        super("No such property '" + propertyName + "' for command '" + commandName + "'");
    }
}
