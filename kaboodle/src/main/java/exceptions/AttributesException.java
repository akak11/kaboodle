package exceptions;

public class AttributesException extends KaboodleException{

    public AttributesException(String message) {
        super(message);
    }
}
