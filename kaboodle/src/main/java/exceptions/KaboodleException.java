package exceptions;

public class KaboodleException extends RuntimeException{

    public KaboodleException(String message) {
        super(message);
    }
}
