package exceptions;

public class NoSuchCommand extends KaboodleException {

    public NoSuchCommand(String illegalCommand) {
        super("No such kaboodle command " + illegalCommand);
    }
}
