package exceptions;

public class IllegalTextInCommand extends KaboodleException{

    public IllegalTextInCommand(String commandName) {
        super("Command '" + commandName + "' can't have text value in tags");
    }
}
