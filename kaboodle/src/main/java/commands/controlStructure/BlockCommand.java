package commands.controlStructure;

import commands.Command;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.List;

public class BlockCommand extends Command {

    private static final String VALUE_FOR_NO_CHILD_COMMANDS = PropertiesReader.getFalseValue();

    private List<Command> childCommands;

    public BlockCommand() {
        this.childCommands = new ArrayList<>();
    }

    public BlockCommand(List<Command> childCommands) {
        this();
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        this.childCommands = childCommands;
    }

    @Override
    public String run() {
        String returnValue = VALUE_FOR_NO_CHILD_COMMANDS;

        for (Command childCommand : this.childCommands) {
            returnValue = childCommand.run();
        }

        return returnValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof BlockCommand)) {
            return false;
        }

        BlockCommand objCommand = (BlockCommand) obj;

        return objCommand.childCommands.equals(this.childCommands);
    }
}
