package commands.controlStructure;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;

import java.util.List;

public class IfCommand extends Command {

    private Command firstCommand;
    private Command secondCommand;
    private Command thirdCommand;

    public IfCommand() {

    }

    public IfCommand(List<Command> childCommands) {
        this();
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 3) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.firstCommand = childCommands.get(0);
        this.secondCommand = childCommands.get(1);
        this.thirdCommand = childCommands.get(2);
    }

    @Override
    public String run() {
        if (this.firstCommand.run().equals(PropertiesReader.getFalseValue())) {
            return this.thirdCommand.run();
        }

        return this.secondCommand.run();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof IfCommand)) {
            return false;
        }

        IfCommand objCommand = (IfCommand) obj;

        return objCommand.firstCommand.equals(this.firstCommand)
                && objCommand.secondCommand.equals(this.secondCommand)
                && objCommand.thirdCommand.equals(this.thirdCommand);
    }
}
