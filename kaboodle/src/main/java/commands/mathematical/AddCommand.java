package commands.mathematical;

import commands.Command;

import java.util.List;

public class AddCommand extends MathematicalCommand {

  public AddCommand() {}

  public AddCommand(List<Command> childCommands) {
    super(childCommands);
  }

  @Override
  public String run() {
    int sum = this.childCommands.stream().map(Command::run).mapToInt(Integer::valueOf).sum();

    return String.valueOf(sum);
  }

  @Override
  public boolean equals(Object obj) {
    return super.equals(obj) && obj instanceof AddCommand;
  }
}
