package commands.mathematical;

import commands.Command;

import java.util.List;

public class MulCommand extends MathematicalCommand {

    public MulCommand() {
    }

    public MulCommand(List<Command> childCommands) {
        super(childCommands);
    }

    @Override
    public String run() {
        int result =
                this.childCommands.stream()
                        .map(Command::run)
                        .mapToInt(Integer::valueOf)
                        .reduce(1, Math::multiplyExact);

        return String.valueOf(result);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof MulCommand;
    }
}
