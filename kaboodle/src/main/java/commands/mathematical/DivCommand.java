package commands.mathematical;

import commands.Command;

import java.util.List;

public class DivCommand extends MathematicalCommand {

    public DivCommand() {
    }

    public DivCommand(List<Command> childCommands) {
        super(childCommands);
    }

    @Override
    public String run() {
        Command firstCommand = this.childCommands.remove(0);
        int firstValue = Integer.parseInt(firstCommand.run());
        int mulResult =
                this.childCommands.stream()
                        .map(Command::run)
                        .mapToInt(Integer::valueOf)
                        .reduce(1, Math::multiplyExact);

        int divResult = firstValue / mulResult;

        return String.valueOf(divResult);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof DivCommand;
    }
}
