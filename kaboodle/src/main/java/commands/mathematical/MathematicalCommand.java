package commands.mathematical;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;

import java.util.ArrayList;
import java.util.List;

public abstract class MathematicalCommand extends Command {

    protected List<Command> childCommands;

    public MathematicalCommand() {
        this.childCommands = new ArrayList<>();
    }

    public MathematicalCommand(List<Command> childCommands) {
        this();
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() < 2) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommands = childCommands;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof MathematicalCommand)) {
            return false;
        }

        MathematicalCommand objCommand = (MathematicalCommand) obj;

        return objCommand.childCommands.equals(this.childCommands);
    }
}
