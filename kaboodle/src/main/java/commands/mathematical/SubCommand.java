package commands.mathematical;

import commands.Command;

import java.util.List;

public class SubCommand extends MathematicalCommand {

    public SubCommand() {
    }

    public SubCommand(List<Command> childCommands) {
        super(childCommands);
    }

    @Override
    public String run() {
        int result = Integer.parseInt(this.childCommands.remove(0).run());

        for (Command childCommand : this.childCommands) {
            result -= Integer.parseInt(childCommand.run());
        }

        return String.valueOf(result);
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj) && obj instanceof SubCommand;
    }
}
