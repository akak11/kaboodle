package commands;

import commands.basic.*;
import commands.controlStructure.BlockCommand;
import commands.controlStructure.IfCommand;
import commands.inputOutput.InputCommand;
import commands.inputOutput.PrintCommand;
import commands.logical.AndCommand;
import commands.logical.NotCommand;
import commands.logical.OrCommand;
import commands.mathematical.*;
import parser.commandFactory.CommandCreator;

public enum CommandType {
    DIV("div", DivCommand::new),
    MOD("mod", ModCommand::new),
    MUL("mul", MulCommand::new),
    NOT("not", NotCommand::new),
    AND("and", AndCommand::new),
    OR("or", OrCommand::new),
    INPUT("input", InputCommand::new),
    PRINT("print", PrintCommand::new),
    BLOCK("block", BlockCommand::new),
    ADD("add", AddCommand::new),
    SUB("sub", SubCommand::new),
    TOHEX("tohex", ToHexCommand::new),
    NOP("nop", NopCommand::new),
    FROMHEX("fromHex", FromHexCommand::new),
    IF("if", IfCommand::new),
    RANDOM("random", RandomCommand::new),
    LITERAL("string", LiteralCommand::new),
    CAT("cat", CatCommand::new),
    EQUALS("equals", EqualsCommand::new);

    private final String commandName;
    private final CommandCreator commandCreator;

    CommandType(String commandName, CommandCreator commandCreator) {
        this.commandName = commandName;
        this.commandCreator = commandCreator;
    }

    public String getCommandName() {
        return commandName;
    }

    public CommandCreator getCommandCreator() {
        return commandCreator;
    }


}
