package commands;

import exceptions.*;
import utils.PropertiesReader;

import java.util.List;
import java.util.Map;

public abstract class Command {

    private final String className = this.getClass().getSimpleName();

    public void updateChildCommands(List<Command> childCommands) {
        if (!childCommands.isEmpty()) {
            throw new IllegalNumberOfChildCommands(this.className, childCommands.size());
        }
    }

    public void updateCommandText(String value) throws IllegalTextInCommand {
        if (value != null) {
            throw new IllegalTextInCommand(this.className);
        }
    }

    public void updateAttributes(Map<String, String> attributes)
            throws AttributesException {
        if (!attributes.isEmpty()) {
            throw new NoSuchAttributeForCommand(this.className, (String) attributes.values().toArray()[0]);
        }
    }

    public String run() {
        return PropertiesReader.getFalseValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        return obj != null && getClass() == obj.getClass();
    }
}
