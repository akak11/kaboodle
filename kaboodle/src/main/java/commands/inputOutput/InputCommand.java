package commands.inputOutput;

import commands.Command;
import utils.io.ConsoleIO;
import utils.io.Input;

public class InputCommand extends Command {

    private final Input input;

    public InputCommand() {
        this.input = new ConsoleIO();
    }

    public InputCommand(Input input) {
        this.input = input;
    }

    @Override
    public String run() {
        return input.readUserInput();
    }
}
