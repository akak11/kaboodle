package commands.inputOutput;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;
import utils.io.ConsoleIO;
import utils.io.Output;

import java.util.List;

public class PrintCommand extends Command {

    private Output output;
    private Command childCommand;

    public PrintCommand() {
        this.output = new ConsoleIO();
    }

    public PrintCommand(Command childCommand) {
        this();
        this.childCommand = childCommand;
    }

    public PrintCommand(Command childCommand, Output output) {
        this(childCommand);
        this.output = output;
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 1) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommand = childCommands.get(0);
    }

    @Override
    public String run() {
        String valueToPrint = this.childCommand.run();
        this.output.printValue(valueToPrint);

        return PropertiesReader.getFalseValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof PrintCommand)) {
            return false;
        }

        PrintCommand objCommand = (PrintCommand) obj;

        return objCommand.childCommand.equals(this.childCommand);
    }
}
