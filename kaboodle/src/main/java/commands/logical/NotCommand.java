package commands.logical;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;

import java.util.List;

public class NotCommand extends Command {

    private Command childCommand;

    public NotCommand() {

    }

    public NotCommand(Command childCommand) {
        this();
        this.childCommand = childCommand;
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 1) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommand = childCommands.get(0);
    }

    @Override
    public String run() {
        String childCommandValue = this.childCommand.run();

        return childCommandValue.equals(PropertiesReader.getFalseValue())
                ? PropertiesReader.getTrueValue()
                : PropertiesReader.getFalseValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof NotCommand)) {
            return false;
        }

        NotCommand objCommand = (NotCommand) obj;

        return objCommand.childCommand.equals(this.childCommand);
    }
}
