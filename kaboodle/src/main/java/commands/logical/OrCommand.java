package commands.logical;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;

import java.util.List;

public class OrCommand extends Command {

    private List<Command> childCommands;

    public OrCommand() {

    }

    public OrCommand(List<Command> childCommands) {
        this();
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() < 2) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommands = childCommands;
    }

    @Override
    public String run() {
        boolean containsTrueValues =
                this.childCommands.stream()
                        .anyMatch(command -> !command.run().equals(PropertiesReader.getFalseValue()));

        return containsTrueValues ? PropertiesReader.getTrueValue() : PropertiesReader.getFalseValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof OrCommand)) {
            return false;
        }

        OrCommand objCommand = (OrCommand) obj;

        return objCommand.childCommands.equals(this.childCommands);
    }
}
