package commands.logical;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;

import java.util.List;

public class AndCommand extends Command {

    private List<Command> childCommands;

    public AndCommand() {

    }

    public AndCommand(List<Command> childCommands) {
        this();
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() < 2) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommands = childCommands;
    }

    @Override
    public String run() {
        boolean containsFalseValues =
                this.childCommands.stream()
                        .anyMatch(command -> command.run().equals(PropertiesReader.getFalseValue()));

        return containsFalseValues ? PropertiesReader.getFalseValue() : PropertiesReader.getTrueValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof AndCommand)) {
            return false;
        }

        AndCommand objCommand = (AndCommand) obj;

        return objCommand.childCommands.equals(this.childCommands);
    }
}
