package commands.basic;

import commands.Command;

public class LiteralCommand extends Command {

    private String value;

    public LiteralCommand() {
        this.value = "";
    }

    public LiteralCommand(String value) {
        this();
        this.value = value;
    }

    @Override
    public void updateCommandText(String value) {
        this.value = value;
    }

    @Override
    public String run() {
        return this.value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof LiteralCommand)) {
            return false;
        }

        LiteralCommand objCommand = (LiteralCommand) obj;

        return objCommand.value.equals(this.value);
    }
}
