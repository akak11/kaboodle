package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;

import java.util.List;

public class FromHexCommand extends Command {

    private Command childCommand;

    public FromHexCommand() {

    }

    public FromHexCommand(Command childCommand) {
        this();
        this.childCommand = childCommand;
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 1) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommand = childCommands.get(0);
    }

    @Override
    public String run() {
        boolean negative = false;
        String origin = this.childCommand.run();

        if (origin.startsWith("-")) {
            origin = origin.substring(1);
            negative = true;
        }

        long childCommandHexValue = Long.parseLong(origin, 16);

        if (negative) {
            childCommandHexValue = -childCommandHexValue;
        }

        return String.valueOf(childCommandHexValue);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof FromHexCommand)) {
            return false;
        }

        FromHexCommand objCommand = (FromHexCommand) obj;

        return objCommand.childCommand.equals(this.childCommand);
    }
}
