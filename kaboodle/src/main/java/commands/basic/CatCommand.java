package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import exceptions.NoSuchAttributeForCommand;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CatCommand extends Command {

    private static final String DEFAULT_SEPARATOR = PropertiesReader.getFalseValue();
    public static final String SEPARATOR_ATTRIBUTE_NAME = "separator";

    private List<Command> childCommands;
    private String separator;

    public CatCommand() {
        this.childCommands = new ArrayList<>();
        this.separator = DEFAULT_SEPARATOR;
    }

    public CatCommand(String separator) {
        this();
        this.separator = separator;
    }

    public CatCommand(List<Command> childCommands) {
        this();
        updateChildCommands(childCommands);
    }

    public CatCommand(String separator, List<Command> childCommands) {
        this(childCommands);
        this.separator = separator;
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() < 2) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommands = childCommands;
    }

    @Override
    public void updateAttributes(Map<String, String> attributes) throws NoSuchAttributeForCommand {
        String separatorAttr = attributes.remove(SEPARATOR_ATTRIBUTE_NAME);
        String className = this.getClass().getSimpleName();

        if (!attributes.isEmpty()) {
            throw new NoSuchAttributeForCommand(className, (String) attributes.values().toArray()[0]);
        }

        this.separator = separatorAttr != null ? separatorAttr : DEFAULT_SEPARATOR;
    }

    @Override
    public String run() {
        List<String> strings =
                this.childCommands.stream().map(Command::run).collect(Collectors.toList());

        return String.join(this.separator, strings);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof CatCommand)) {
            return false;
        }

        CatCommand objCommand = (CatCommand) obj;

        return objCommand.childCommands.equals(this.childCommands);
    }
}
