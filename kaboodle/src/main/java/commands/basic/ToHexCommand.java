package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;

import java.util.List;

public class ToHexCommand extends Command {

    private Command childCommand;

    public ToHexCommand() {
    }

    public ToHexCommand(Command childCommand) {
        this();
        this.childCommand = childCommand;
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 1) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.childCommand = childCommands.get(0);
    }

    @Override
    public String run() {
        String origin = this.childCommand.run();
        boolean negative = false;

        if (origin.startsWith("-")) {
            negative = true;
            origin = origin.substring(1);
        }

        long childCommandValue = Long.parseLong(origin);
        String hexString = Long.toHexString(childCommandValue);

        if (negative) {
            hexString = "-" + hexString;
        }

        return hexString;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ToHexCommand)) {
            return false;
        }

        ToHexCommand objCommand = (ToHexCommand) obj;

        return objCommand.childCommand.equals(this.childCommand);
    }
}
