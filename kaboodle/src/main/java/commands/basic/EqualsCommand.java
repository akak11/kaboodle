package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import utils.PropertiesReader;

import java.util.List;

public class EqualsCommand extends Command {

    private static final String TRUE_VALUE = PropertiesReader.getTrueValue();
    private static final String FALSE_VALUE = PropertiesReader.getFalseValue();

    private Command firstCommand;
    private Command secondCommand;

    public EqualsCommand() {
    }

    public EqualsCommand(List<Command> childCommands) {
        this.updateChildCommands(childCommands);
    }

    @Override
    public void updateChildCommands(List<Command> childCommands) {
        String className = this.getClass().getSimpleName();

        if (childCommands.size() != 2) {
            throw new IllegalNumberOfChildCommands(className, childCommands.size());
        }

        this.firstCommand = childCommands.get(0);
        this.secondCommand = childCommands.get(1);
    }

    @Override
    public String run() {
        String firstCommandValue = this.firstCommand.run();
        String secondCommandValue = this.secondCommand.run();
        boolean areCommandsEqual = firstCommandValue.equals(secondCommandValue);

        return areCommandsEqual ? TRUE_VALUE : FALSE_VALUE;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof EqualsCommand)) {
            return false;
        }

        EqualsCommand objCommand = (EqualsCommand) obj;

        return objCommand.firstCommand.equals(this.firstCommand)
                && objCommand.secondCommand.equals(this.secondCommand);
    }
}
