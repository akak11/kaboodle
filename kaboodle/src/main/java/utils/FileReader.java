package utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileReader {

    public String readFromFileToString(String fileName) throws IOException {
        return Files.readString(Path.of(fileName));
    }
}
