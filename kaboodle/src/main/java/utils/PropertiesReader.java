package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesReader {

    public static final String PROPERTIES_FILE_NAME = "kaboodle.properties";
    public static final String FALSE_PROPERTY_NAME = "False";
    public static final String TRUE_PROPERTY_NAME = "True";
    public static final Properties properties = new Properties();

    private static void loadProperties() {
        try (InputStream iStream =
                     PropertiesReader.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME)) {
            properties.load(iStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getFalseValue() {
        if (properties.isEmpty()) {
            loadProperties();
        }

        return properties.getProperty(FALSE_PROPERTY_NAME);
    }

    public static String getTrueValue() {
        if (properties.isEmpty()) {
            loadProperties();
        }

        return properties.getProperty(TRUE_PROPERTY_NAME);
    }
}
