package utils.io;

public interface Input {

    String readUserInput();
}
