package utils.io;

import java.util.Scanner;

public class ConsoleIO implements Output, Input {

    private final Scanner scanner = new Scanner(System.in);

    @Override
    public String readUserInput() {
        return scanner.nextLine();
    }

    @Override
    public void printValue(String value) {
        System.out.println(value);
    }
}
