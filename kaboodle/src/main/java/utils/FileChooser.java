package utils;

import javax.swing.*;
import java.awt.*;
import java.io.File;

public class FileChooser extends Component {

    public String chooseFile() {
        String selectedFileName = "";
        JFileChooser fileChooser = new JFileChooser();
        String directory = "user.home";
        fileChooser.setCurrentDirectory(new File(System.getProperty(directory)));
        int result = fileChooser.showDialog(this, "Choose kaboodle file");

        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = fileChooser.getSelectedFile();
            selectedFileName = selectedFile.getAbsolutePath();
        }

        return selectedFileName;
    }
}
