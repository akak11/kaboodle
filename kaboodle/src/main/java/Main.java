import commands.Command;
import org.w3c.dom.Document;
import parser.CommandsExecutor;
import parser.DOMCreator;
import parser.DOMParser;
import parser.commandFactory.CommandFactory;
import utils.FileChooser;
import utils.FileReader;

public class Main {

    public static void main(String[] args) throws Exception {
        FileChooser fileChooser = new FileChooser();
        String fileName;
        if (args.length == 0) {
            fileName = fileChooser.chooseFile();
        } else {
            fileName = args[0];
        }

        if (fileName.isEmpty()) {
            System.out.println("File not providedm" +
                    "" +
                    "] ,      , exiting program");
        } else {
            FileReader fileReader = new FileReader();
            String code = fileReader.readFromFileToString(fileName);

            DOMCreator domCreator = new DOMCreator();
            Document dom = domCreator.createFromString(code);

            DOMParser domParser = new DOMParser(new CommandFactory());
            Command commands = domParser.createCommandsFromDom(dom);

            CommandsExecutor commandsExecutor = new CommandsExecutor();
            commandsExecutor.execute(commands);
        }
    }
}
