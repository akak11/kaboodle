package parser;

import commands.Command;
import exceptions.AttributesException;
import exceptions.KaboodleException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import parser.commandFactory.CommandFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DOMParser {

    private final CommandFactory commandFactory;

    public DOMParser(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;
    }

    public Command createCommandsFromDom(Document codeDOM) throws KaboodleException {
        return createCommand(codeDOM.getFirstChild());
    }

    public Command createCommand(Node node)
            throws KaboodleException {
        Command command = this.commandFactory.getCommand(node.getNodeName());

        updateCommandAttributes(node, command);
        updateChildCommands(node.getChildNodes(), command);

        return command;
    }

    public void updateChildCommands(NodeList childCommandNodes, Command command) {
        List<Command> childCommands = new ArrayList<>();

        for (int childNodeIndex = 0; childNodeIndex < childCommandNodes.getLength(); childNodeIndex++) {
            Node currChildNode = childCommandNodes.item(childNodeIndex);

            if (currChildNode.getNodeType() == Node.TEXT_NODE) {
                command.updateCommandText(currChildNode.getNodeValue());
            } else if (currChildNode.getNodeType() == Node.ELEMENT_NODE){
                childCommands.add(this.createCommand(currChildNode));
            }
        }

        command.updateChildCommands(childCommands);
    }


    private void updateCommandAttributes(Node node, Command command)
            throws AttributesException {
        Map<String, String> attributes = new HashMap<>();

        if (node.hasAttributes()) {
            attributes = createAttributesMap(node);
        }

        command.updateAttributes(attributes);
    }

    private Map<String, String> createAttributesMap(Node node) {
        Map<String, String> attributes = new HashMap<>();

        NamedNodeMap attributeNodes = node.getAttributes();

        for (int attributeIndex = 0; attributeIndex < attributeNodes.getLength(); attributeIndex++) {
            Node currAttribute = attributeNodes.item(attributeIndex);
            attributes.put(currAttribute.getNodeName().toLowerCase(), currAttribute.getNodeValue());
        }

        return attributes;
    }
}
