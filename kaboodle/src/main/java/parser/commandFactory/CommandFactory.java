package parser.commandFactory;

import commands.Command;
import commands.CommandType;
import exceptions.NoSuchCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {

    private final Map<String, CommandCreator> COMMAND_FACTORY_MAP;

    public CommandFactory() {
        this.COMMAND_FACTORY_MAP = new HashMap<>();
        for (CommandType command : CommandType.values()) {
            COMMAND_FACTORY_MAP.put(command.getCommandName(), command.getCommandCreator());
        }
    }

    public Command getCommand(String commandName) throws NoSuchCommand {
        if (!COMMAND_FACTORY_MAP.containsKey(commandName.toLowerCase())) {
            throw new NoSuchCommand(commandName);
        }

        return COMMAND_FACTORY_MAP.get(commandName.toLowerCase()).create();
    }
}
