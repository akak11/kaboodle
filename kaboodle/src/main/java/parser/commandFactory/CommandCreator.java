package parser.commandFactory;

import commands.Command;

public interface CommandCreator {
    Command create();
}
