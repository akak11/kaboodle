package parser;

import commands.CommandType;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class DOMCreator {

    public Document createFromString(String xmlString)
            throws ParserConfigurationException, IOException, SAXException {
        xmlString = removeWhitespace(xmlString);
        String DOMString = "<" + CommandType.BLOCK.getCommandName() + ">" + xmlString + "</" + CommandType.BLOCK.getCommandName() + ">";

        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(DOMString));

        return builder.parse(is);
    }


    private String removeWhitespace(String text) {
        return text.replaceAll("[\\t\\n\\r]", "");
    }
}
