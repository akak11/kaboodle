package parser;

import commands.Command;

public class CommandsExecutor {
    public void execute(Command commands) {
        commands.run();
    }
}
