package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FromHexCommandTest {

  @Test
  public void
      givenCommandWithOnePositiveChildCommand_whenRunningCommand_thenReturnedDecimalValue() {
    final String mockReturnValue = "FF";
    final Command commandMock = MockBuilder.getMock(mockReturnValue);

    FromHexCommand fromHexCommand = new FromHexCommand(commandMock);
    String value = fromHexCommand.run();

    final String expected = "255";
    assertEquals(expected, value);
  }

  @Test
  public void
      givenCommandWithOneNegativeChildCommand_whenRunningCommand_thenReturnedDecimalValue() {
    final String mockReturnValue = "-da";
    final Command commandMock = MockBuilder.getMock(mockReturnValue);

    FromHexCommand fromHexCommand = new FromHexCommand(commandMock);
    String value = fromHexCommand.run();

    final String expected = "-218";
    assertEquals(expected, value);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    List<Command> childCommands = new ArrayList<>();
    FromHexCommand command = new FromHexCommand();

    command.updateChildCommands(childCommands);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
    FromHexCommand command = new FromHexCommand();

    command.updateChildCommands(childCommands);
  }

  @Test
  public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    Command childCommand = MockBuilder.getMock(mockReturnValue);
    FromHexCommand command = new FromHexCommand();

    command.updateChildCommands(List.of(childCommand));

    FromHexCommand expected = new FromHexCommand(childCommand);
    assertEquals(expected, command);
  }
}
