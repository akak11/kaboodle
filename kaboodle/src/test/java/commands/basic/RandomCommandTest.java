package commands.basic;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RandomCommandTest {

    private final Random random = Mockito.mock(Random.class);

    @Test
    public void
    givenRandomCommandWithMinAndMaxAttributes_whenRunningCommand_thenGenerateRandomNumberInCorrectRange() {
        final int min = -5;
        final int max = 2;
        final int range = max - min + 1;

        RandomCommand randomCommand = new RandomCommand(random, min, max);
        randomCommand.run();

        verify(random).nextInt(range);
    }

    @Test
    public void
    givenRandomCommandWithMinAndMaxAttributes_whenRunningCommandAndRandomClassReturnsMaxValue_thenRandomCommandReturnsMax() {
        final int min = -5;
        final int max = 2;
        final int range = max - min + 1;
        RandomCommand randomCommand = new RandomCommand(random, min, max);

        when(random.nextInt(range)).thenReturn(range - 1);
        int randomResult = Integer.parseInt(randomCommand.run());

        assertEquals(max, randomResult);
    }

    @Test
    public void
    givenRandomCommandWithOnlyMaxAttribute_whenRunningCommand_thenGenerateRandomNumberInCorrectRange() {
        final int max = 7;
        final int range = max - RandomCommand.DEFAULT_MIN + 1;
        RandomCommand randomCommand = new RandomCommand(random, max);

        randomCommand.run();

        verify(random).nextInt(range);
    }
}
