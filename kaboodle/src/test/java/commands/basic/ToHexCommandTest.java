package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ToHexCommandTest {

  @Test
  public void
      givenCommandWithOnePositiveChildCommand_whenRunningCommand_thenReturnedHexValueLowercase() {
    final String decimalString = "763";
    final Command commandMock = MockBuilder.getMock(decimalString);

    ToHexCommand ToHexCommand = new ToHexCommand(commandMock);
    String value = ToHexCommand.run();

    final String expected = "2fb";
    assertEquals(expected, value);
  }

  @Test
  public void
      givenCommandWithOneNegativeChildCommand_whenRunningCommand_thenReturnedHexValueLowercase() {
    final String decimalString = "-763";
    final Command commandMock = MockBuilder.getMock(decimalString);

    ToHexCommand ToHexCommand = new ToHexCommand(commandMock);
    String value = ToHexCommand.run();

    final String expected = "-2fb";
    assertEquals(expected, value);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    List<Command> childCommands = new ArrayList<>();
    ToHexCommand command = new ToHexCommand();

    command.updateChildCommands(childCommands);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
    ToHexCommand command = new ToHexCommand();

    command.updateChildCommands(childCommands);
  }

  @Test
  public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    Command childCommand = MockBuilder.getMock(mockReturnValue);
    ToHexCommand command = new ToHexCommand();

    command.updateChildCommands(List.of(childCommand));

    ToHexCommand expected = new ToHexCommand(childCommand);
    assertEquals(expected, command);
  }
}
