package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class EqualsCommandTest {

    @Test
    public void
    givenEqualsCommandWithSameReturnValueSameCaseChildCommands_whenRunningCommand_thenReturn1() {
        final String firstMockReturnValue = "test";
        final String secondMockReturnValue = "test";

        List<Command> childCommands =
                MockBuilder.getMockList(List.of(firstMockReturnValue, secondMockReturnValue));
        EqualsCommand equalsCommand = new EqualsCommand(childCommands);

        String expected = PropertiesReader.getTrueValue();
        assertEquals(expected, equalsCommand.run());
    }

    @Test
    public void
    givenEqualsCommandWithSameReturnValueDifferentCaseChildCommands_whenRunningCommand_thenReturnFalseValue() {
        final String firstMockReturnValue = "test";
        final String secondMockReturnValue = "TEST";

        List<Command> childCommands =
                MockBuilder.getMockList(List.of(firstMockReturnValue, secondMockReturnValue));
        EqualsCommand equalsCommand = new EqualsCommand(childCommands);

        String expected = PropertiesReader.getFalseValue();
        assertEquals(expected, equalsCommand.run());
    }

    @Test
    public void
    givenEqualsCommandWithDifferentReturnValueChildCommands_whenRunningCommand_thenReturnFalseValue() {
        final String firstMockReturnValue = "test";
        final String secondMockReturnValue = "tts";

        List<Command> childCommands =
                MockBuilder.getMockList(List.of(firstMockReturnValue, secondMockReturnValue));
        EqualsCommand equalsCommand = new EqualsCommand(childCommands);

        String expected = PropertiesReader.getFalseValue();
        assertEquals(expected, equalsCommand.run());
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue));
        EqualsCommand command = new EqualsCommand();

        command.updateChildCommands(childCommands);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
        EqualsCommand command = new EqualsCommand();

        command.updateChildCommands(childCommands);
    }

    @Test
    public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue));
        EqualsCommand command = new EqualsCommand();

        command.updateChildCommands(childCommands);

        EqualsCommand expected = new EqualsCommand(childCommands);
        assertEquals(expected, command);
    }

}
