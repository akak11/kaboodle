package commands.basic;

import org.junit.Test;
import utils.PropertiesReader;

import static org.junit.Assert.assertEquals;

public class NopCommandTest {

  @Test
  public void givenNopCommand_whenRunningCommand_thenReturnFalseValue() {
    NopCommand nopCommand = new NopCommand();

    String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, nopCommand.run());
  }
}
