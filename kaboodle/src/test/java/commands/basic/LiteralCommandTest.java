package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LiteralCommandTest {

  @Test
  public void givenLiteralCommandWithValue_whenRunningCommand_thenReturnValue() {
    final String literalCommandValue = "my first test";
    LiteralCommand literalCommand = new LiteralCommand(literalCommandValue);
    String value = literalCommand.run();

    assertEquals(literalCommandValue, value);
  }

  @Test
  public void givenLiteralCommandWithFalseValue_whenRunningCommand_thenReturnFalseString() {
    final String literalCommandFalseValue = PropertiesReader.getFalseValue();
    LiteralCommand literalCommand = new LiteralCommand(literalCommandFalseValue);
    String value = literalCommand.run();

    assertEquals(literalCommandFalseValue, value);
  }


  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue, mockReturnValue));
    LiteralCommand command = new LiteralCommand();

    command.updateChildCommands(childCommands);
  }

  @Test
  public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    List<Command> childCommand = new ArrayList<>();
    LiteralCommand command = new LiteralCommand();

    command.updateChildCommands(childCommand);

    LiteralCommand expected = new LiteralCommand();
    assertEquals(expected, command);
  }
}
