package commands.basic;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import exceptions.NoSuchAttributeForCommand;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class CatCommandTest {

    @Test
    public void givenCatCommandWithTwoCommands_whenRunningCommand_thenReturnConcatenatedString() {
        String firstMockReturnValue = "my";
        String secondMockReturnValue = "test";
        List<Command> childCommands =
                MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));

        CatCommand catCommand = new CatCommand(childCommands);

        String value = catCommand.run();

        final String expected = "mytest";
        assertEquals(expected, value);
    }

    @Test
    public void
    givenCatCommandWithTwoCommandsAndSeparator_whenRunningCommand_thenReturnConcatenatedString() {
        String firstMockReturnValue = "my";
        String secondMockReturnValue = "test";

        List<Command> childCommands =
                MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
        CatCommand catCommand = new CatCommand(" ", childCommands);

        String value = catCommand.run();

        final String expected = "my test";
        assertEquals(expected, value);
    }

    @Test
    public void
    givenCatCommandWithMultipleCommands_whenRunningCommand_thenReturnConcatenatedString() {
        String firstMockReturnValue = "my";
        String secondMockReturnValue = "cool";
        String thirdMockReturnValue = "test";

        List<Command> childCommands =
                MockBuilder.getMockList(
                        Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
        CatCommand catCommand = new CatCommand(childCommands);

        String value = catCommand.run();

        final String expected = "mycooltest";
        assertEquals(expected, value);
    }

    @Test
    public void
    givenCatCommandWithMultipleCommandsAndSeparator_whenRunningCommand_thenReturnConcatenatedString() {
        String firstMockReturnValue = "my";
        String secondMockReturnValue = "cool";
        String thirdMockReturnValue = "test";

        List<Command> childCommands =
                MockBuilder.getMockList(
                        Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
        CatCommand catCommand = new CatCommand(" ", childCommands);

        String value = catCommand.run();

        final String expected = "my cool test";
        assertEquals(expected, value);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenListOfTooLittleChildCommands_whenUpdatingChildCommands_thenThrowException() {
        CatCommand command = new CatCommand();
        List<Command> childCommands = new ArrayList<>();

        command.updateChildCommands(childCommands);
    }

    @Test()
    public void givenListOfTheRightAmountOfChildCommands_whenUpdatingChildCommands_thenUpdateCommand() {
        CatCommand command = new CatCommand();
        List<Command> childCommands = List.of(new LiteralCommand("hello"), new LiteralCommand("hi|"));

        command.updateChildCommands(childCommands);

        CatCommand expected = new CatCommand(childCommands);
        assertEquals(expected, command);
    }

    @Test(expected = NoSuchAttributeForCommand.class)
    public void givenListOfWithIllegalAttribute_whenUpdatingAttributes_thenThrowException() {
        CatCommand command = new CatCommand();
        Map<String, String> attributes = new HashMap<>();
        attributes.put("min", "34");

        command.updateAttributes(attributes);
    }

    @Test()
    public void givenListOfTheRightAttribute_whenUpdatingAttributes_thenUpdateCommand() {
        String separator = " ";
        CatCommand command = new CatCommand();
        Map<String, String> attributes = new HashMap<>();
        attributes.put("separator", separator);

        command.updateAttributes(attributes);

        CatCommand expected = new CatCommand(separator);
        assertEquals(expected, command);
    }
}
