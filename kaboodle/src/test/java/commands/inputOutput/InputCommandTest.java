package commands.inputOutput;

import org.junit.Test;
import org.mockito.Mockito;
import utils.io.Input;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class InputCommandTest {

    private final Input io = Mockito.mock(Input.class);

    @Test
    public void givenInputCommand_whenUserInsertsInput_thenReturnUserInput() {
        final String userInput = "hello";
        when(io.readUserInput()).thenReturn(userInput);

        InputCommand inputCommand = new InputCommand(io);
        String returnedValue = inputCommand.run();

        assertEquals(userInput, returnedValue);
    }
}
