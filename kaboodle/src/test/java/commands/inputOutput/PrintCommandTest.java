package commands.inputOutput;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import org.mockito.Mockito;
import testingUtils.MockBuilder;
import utils.PropertiesReader;
import utils.io.Output;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

public class PrintCommandTest {

    private final Output io = Mockito.mock(Output.class);

    @Test
    public void
    givenPrintCommandWithOneChild_whenRunningCommand_thenCommandPrintsChildCommandsReturnValue() {
        String mockReturnValue = "messageToPrint";
        final Command commandMock = MockBuilder.getMock(mockReturnValue);

        PrintCommand printCommand = new PrintCommand(commandMock, io);
        printCommand.run();

        verify(io).printValue(mockReturnValue);
    }

    @Test
    public void
    givenPrintCommandWithOneChild_whenRunningCommand_thenCommandReturnsFalseValueString() {
        String mockReturnValue = "messageToPrint";
        final Command commandMock = MockBuilder.getMock(mockReturnValue);

        PrintCommand printCommand = new PrintCommand(commandMock);
        String returnValue = printCommand.run();

        assertEquals(PropertiesReader.getFalseValue(), returnValue);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        List<Command> childCommands = new ArrayList<>();
        PrintCommand command = new PrintCommand();

        command.updateChildCommands(childCommands);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
        PrintCommand command = new PrintCommand();

        command.updateChildCommands(childCommands);
    }

    @Test
    public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        Command childCommand = MockBuilder.getMock(mockReturnValue);
        PrintCommand command = new PrintCommand();

        command.updateChildCommands(List.of(childCommand));

        PrintCommand expected = new PrintCommand(childCommand);
        assertEquals(expected, command);
    }
}
