package commands.logical;

import commands.Command;
import commands.basic.LiteralCommand;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OrCommandTest {

  @Test
  public void
      givenOrCommandWithTwoChildCommandsThatHaveFalseValues_whenRunningCommand_thenReturnFalseValue() {
    final String firstMockReturnValue = PropertiesReader.getFalseValue();
    final String secondMockReturnValue = PropertiesReader.getFalseValue();

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    OrCommand orCommand = new OrCommand(childCommands);

    String returnValue = orCommand.run();
    String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenOrCommandWithMultipleChildCommandsThatHaveFalseValues_whenRunningCommand_thenReturnFalseValue() {
    final String MockReturnValue = PropertiesReader.getFalseValue();

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(MockReturnValue, MockReturnValue, MockReturnValue));
    OrCommand orCommand = new OrCommand(childCommands);

    String returnValue = orCommand.run();
    String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenOrCommandWithTwoChildCommandsThatDontHaveFalseValues_whenRunningCommand_thenReturnNotFalseString() {
    final String firstMockReturnValue = "lkdfc";
    final String secondMockReturnValue = PropertiesReader.getFalseValue();

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    OrCommand orCommand = new OrCommand(childCommands);

    String returnValue = orCommand.run();
    String expected = PropertiesReader.getTrueValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenOrCommandWithMultipleChildCommandsThatDontHaveFalseValues_whenRunningCommand_thenReturnNotFalseString() {
    final String firstMockReturnValue = "lkdfc";
    final String secondMockReturnValue = PropertiesReader.getFalseValue();
    final String thirdMockReturnValue = PropertiesReader.getFalseValue();

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    OrCommand orCommand = new OrCommand(childCommands);

    String returnValue = orCommand.run();
    String expected = PropertiesReader.getTrueValue();
    assertEquals(expected, returnValue);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenListOfTooLittleChildCommands_whenUpdatingChildCommands_thenThrowException() {
    OrCommand command = new OrCommand();
    List<Command> childCommands = new ArrayList<>();

    command.updateChildCommands(childCommands);
  }

  @Test()
  public void givenListOfTheRightAmountOfChildCommands_whenUpdatingChildCommands_thenUpdateCommand() {
    OrCommand command = new OrCommand();
    List<Command> childCommands = List.of(new LiteralCommand("hello"), new LiteralCommand("hi|"));

    command.updateChildCommands(childCommands);

    OrCommand expected = new OrCommand(childCommands);
    assertEquals(expected, command);
  }
}
