package commands.logical;

import commands.Command;
import commands.basic.LiteralCommand;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AndCommandTest {

  @Test
  public void
      givenAndCommandWithTwoChildCommandsThatDontHaveFalseValues_whenRunningCommand_thenReturnNotFalseString() {
    final String firstMockReturnValue = "sf";
    final String secondMockReturnValue = "sc";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    AndCommand andCommand = new AndCommand(childCommands);

    String returnValue = andCommand.run();
    String expected = PropertiesReader.getTrueValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenAndCommandWithMultipleChildCommandsThatDontHaveFalseValues_whenRunningCommand_thenReturnNotFalseString() {
    final String MockReturnValue = "efse";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(MockReturnValue, MockReturnValue, MockReturnValue));
    AndCommand andCommand = new AndCommand(childCommands);

    String returnValue = andCommand.run();
    String expected = PropertiesReader.getTrueValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenAndCommandWithTwoChildCommandsOneHasFalseValue_whenRunningCommand_thenReturnFalseString() {
    final String firstMockReturnValue = "lkdfc";
    final String secondMockReturnValue = PropertiesReader.getFalseValue();
    final String thirdMockReturnValue = "2l3k";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    AndCommand andCommand = new AndCommand(childCommands);

    String returnValue = andCommand.run();
    String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, returnValue);
  }

  @Test
  public void
      givenAndCommandWithMultipleChildCommandsOneHasFalseValue_whenRunningCommand_thenReturnFalseString() {
    final String firstMockReturnValue = "lkdfc";
    final String secondMockReturnValue = "erd";
    final String thirdMockReturnValue = PropertiesReader.getFalseValue();

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    AndCommand andCommand = new AndCommand(childCommands);

    String returnValue = andCommand.run();
    String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, returnValue);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenListOfTooLittleChildCommands_whenUpdatingChildCommands_thenThrowException() {
    AndCommand command = new AndCommand();
    List<Command> childCommands = new ArrayList<>();

    command.updateChildCommands(childCommands);
  }

  @Test()
  public void givenListOfTheRightAmountOfChildCommands_whenUpdatingChildCommands_thenUpdateCommand() {
    AndCommand command = new AndCommand();
    List<Command> childCommands = List.of(new LiteralCommand("hello"), new LiteralCommand("hi|"));

    command.updateChildCommands(childCommands);

    AndCommand expected = new AndCommand(childCommands);
    assertEquals(expected, command);
  }
}
