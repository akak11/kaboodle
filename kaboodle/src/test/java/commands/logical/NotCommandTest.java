package commands.logical;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class NotCommandTest {

  @Test
  public void
      givenNotCommandWithChildCommandValueFalse_whenRunningCommand_thenReturnNotCommandsRelevantValue() {
    String mockReturnValue = PropertiesReader.getFalseValue();
    final Command commandMock = MockBuilder.getMock(mockReturnValue);

    NotCommand notCommand = new NotCommand(commandMock);
    String returnValue = notCommand.run();

    assertEquals(PropertiesReader.getTrueValue(), returnValue);
  }

  @Test
  public void
      givenNotCommandWithChildCommandTruthyValue_whenRunningCommand_thenReturnNotCommandsRelevantValue() {
    String mockReturnValue = "something";
    final Command commandMock = MockBuilder.getMock(mockReturnValue);

    NotCommand notCommand = new NotCommand(commandMock);
    String returnValue = notCommand.run();

    assertEquals(PropertiesReader.getFalseValue(), returnValue);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    List<Command> childCommands = new ArrayList<>();
    NotCommand command = new NotCommand();

    command.updateChildCommands(childCommands);
  }

  @Test(expected = IllegalNumberOfChildCommands.class)
  public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
    NotCommand command = new NotCommand();

    command.updateChildCommands(childCommands);
  }

  @Test
  public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
    String mockReturnValue = "hi";
    Command childCommand = MockBuilder.getMock(mockReturnValue);
    NotCommand command = new NotCommand();

    command.updateChildCommands(List.of(childCommand));

    NotCommand expected = new NotCommand(childCommand);
    assertEquals(expected, command);
  }
}
