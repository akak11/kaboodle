package commands.controlStructure;

import commands.Command;
import org.junit.Test;
import org.mockito.InOrder;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.inOrder;

public class BlockCommandTest {

  @Test
  public void givenBlockCommandWithZeroChildCommands_whenRunningCommand_thenReturnFalseValue() {
    BlockCommand blockCommand = new BlockCommand();

    final String expected = PropertiesReader.getFalseValue();
    assertEquals(expected, blockCommand.run());
  }

  @Test
  public void
      givenBlockCommandWithOneChildCommand_whenRunningCommand_thenReturnValueOfChildCommand() {
    final String mockReturnValue = "my";

    List<Command> childCommands =
        MockBuilder.getMockList(Collections.singletonList(mockReturnValue));
    BlockCommand blockCommand = new BlockCommand(childCommands);

    assertEquals(mockReturnValue, blockCommand.run());
  }

  @Test
  public void
      givenBlockCommandWithMultipleChildCommands_whenRunningCommand_thenReturnValueOfLastChildCommand() {
    final String firstMockReturnValue = "first command";
    final String secondMockReturnValue = "second command";
    final String thirdMockReturnValue = "last command";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    BlockCommand blockCommand = new BlockCommand(childCommands);

    assertEquals(thirdMockReturnValue, blockCommand.run());
  }

  @Test
  public void
      givenBlockCommandWithMultipleChildCommands_whenRunningCommand_thenCommandsRunInOrderBeforeReturningLastValue() {
    final String firstMockReturnValue = "first command";
    final String secondMockReturnValue = "second command";
    final String thirdMockReturnValue = "last command";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    InOrder inOrder = inOrder(childCommands.get(0), childCommands.get(1), childCommands.get(2));

    BlockCommand blockCommand = new BlockCommand(childCommands);

    blockCommand.run();
    inOrder.verify(childCommands.get(0)).run();
    inOrder.verify(childCommands.get(1)).run();
    inOrder.verify(childCommands.get(2)).run();
  }
}
