package commands.controlStructure;

import commands.Command;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import testingUtils.MockBuilder;
import utils.PropertiesReader;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class IfCommandTest {

    @Test
    public void givenIfCommandWhereFirstCommandValueIsFalseString_whenRunningCommand_thenReturnValueOfThirdCommand() {
        final String firstMockReturnValue = PropertiesReader.getFalseValue();
        final String secondMockReturnValue = "second command";
        final String lastMockReturnValue = "third command";

        List<Command> childCommands = MockBuilder.getMockList(List.of(firstMockReturnValue, secondMockReturnValue, lastMockReturnValue));
        IfCommand ifCommand = new IfCommand(childCommands);

        assertEquals(lastMockReturnValue, ifCommand.run());
    }

    @Test
    public void givenIfCommandWhereFirstCommandValueIsNotFalseString_whenRunningCommand_thenReturnValueOfSecondCommand() {
        final String firstMockReturnValue = "not empty string";
        final String secondMockReturnValue = "second command";
        final String lastMockReturnValue = "third command";

        List<Command> childCommands = MockBuilder.getMockList(List.of(firstMockReturnValue, secondMockReturnValue, lastMockReturnValue));
        IfCommand ifCommand = new IfCommand(childCommands);

        assertEquals(secondMockReturnValue, ifCommand.run());
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenLessThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        List<Command> childCommands = new ArrayList<>();
        IfCommand command = new IfCommand();

        command.updateChildCommands(childCommands);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenMoreThanNeededAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommands = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue, mockReturnValue));
        IfCommand command = new IfCommand();

        command.updateChildCommands(childCommands);
    }

    @Test
    public void givenRightAmountOfChildCommands_whenUpdatingChildCommands_thenThrowException() {
        String mockReturnValue = "hi";
        List<Command> childCommand = MockBuilder.getMockList(List.of(mockReturnValue, mockReturnValue, mockReturnValue));
        IfCommand command = new IfCommand();

        command.updateChildCommands(childCommand);

        IfCommand expected = new IfCommand(childCommand);
        assertEquals(expected, command);
    }
}
