package commands.mathematical;

import commands.Command;
import commands.basic.LiteralCommand;
import exceptions.IllegalNumberOfChildCommands;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doCallRealMethod;

public class MathematicalCommandTest {

    private final MathematicalCommand mathematicalCommand = Mockito.mock(MathematicalCommand.class);

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void givenListOfTooLittleChildCommands_whenUpdatingChildCommands_thenThrowException() {
        List<Command> childCommands = new ArrayList<>();

        doCallRealMethod().when(mathematicalCommand).updateChildCommands(childCommands);

        mathematicalCommand.updateChildCommands(childCommands);
    }

    @Test()
    public void givenListOfTheRightAmountOfChildCommands_whenUpdatingChildCommands_thenUpdateCommand() {
        List<Command> childCommands = List.of(new LiteralCommand("hello"), new LiteralCommand("hi|"));
        doCallRealMethod().when(mathematicalCommand).updateChildCommands(childCommands);

        mathematicalCommand.updateChildCommands(childCommands);

        assertEquals(2, mathematicalCommand.childCommands.size());
    }
}
