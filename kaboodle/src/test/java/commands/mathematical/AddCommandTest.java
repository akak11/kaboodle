package commands.mathematical;

import commands.Command;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AddCommandTest {

  @Test
  public void
      givenAddCommandWithTwoChildCommands_whenRunningCommand_thenReturnSumOfChildCommandsValues() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "5";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    AddCommand addCommand = new AddCommand(childCommands);
    String sum = addCommand.run();

    String expected = "7";
    assertEquals(expected, sum);
  }

  @Test
  public void
      givenAddCommandWithMultipleChildCommands_whenRunningCommand_thenReturnSumOfChildCommandsValues() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "5";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));

    AddCommand addCommand = new AddCommand(childCommands);
    String sum = addCommand.run();

    String expected = "10";
    assertEquals(expected, sum);
  }

  @Test(expected = NumberFormatException.class)
  public void
      givenAddCommandWithChildCommandNotReturningStringNumber_whenRunningCommand_thenThrowsRuntimeException() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "R";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));

    AddCommand addCommand = new AddCommand(childCommands);
    addCommand.run();
  }
}
