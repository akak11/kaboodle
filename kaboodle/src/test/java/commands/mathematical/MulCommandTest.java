package commands.mathematical;

import commands.Command;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MulCommandTest {

  @Test
  public void
      givenMulCommandWithTwoChildCommands_whenRunningCommand_thenReturnMulOfChildCommandsValues() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "5";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    MulCommand mulCommand = new MulCommand(childCommands);
    String sum = mulCommand.run();

    String expected = "10";
    assertEquals(expected, sum);
  }

  @Test
  public void
      givenMulCommandWithMultipleChildCommands_whenRunningCommand_thenReturnMulOfChildCommandsValues() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "5";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    MulCommand mulCommand = new MulCommand(childCommands);
    String sum = mulCommand.run();

    String expected = "30";
    assertEquals(expected, sum);
  }

  @Test(expected = NumberFormatException.class)
  public void
      givenMulCommandWithChildCommandNotReturningStringNumber_whenRunningCommand_thenThrowsRuntimeException() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "R";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    MulCommand mulCommand = new MulCommand(childCommands);
    mulCommand.run();
  }
}
