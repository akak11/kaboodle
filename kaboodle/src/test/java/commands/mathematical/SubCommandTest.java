package commands.mathematical;

import commands.Command;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SubCommandTest {

  @Test
  public void
      givenSubCommandWithTwoChildCommands_whenRunningCommand_thenReturnSubOfChildCommandsValues() {
    final String firstMockReturnValue = "5";
    final String secondMockReturnValue = "2";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    SubCommand subCommand = new SubCommand(childCommands);
    String sum = subCommand.run();

    String expected = "3";
    assertEquals(expected, sum);
  }

  @Test
  public void
      givenSubCommandWithMultipleChildCommands_whenRunningCommand_thenReturnSubOfChildCommandsValues() {
    final String firstMockReturnValue = "10";
    final String secondMockReturnValue = "5";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    SubCommand subCommand = new SubCommand(childCommands);
    String sum = subCommand.run();

    String expected = "2";
    assertEquals(expected, sum);
  }

  @Test(expected = NumberFormatException.class)
  public void
      givenSubCommandWithChildCommandNotReturningStringNumber_whenRunningCommand_thenThrowsRuntimeException() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "R";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    SubCommand subCommand = new SubCommand(childCommands);
    subCommand.run();
  }
}
