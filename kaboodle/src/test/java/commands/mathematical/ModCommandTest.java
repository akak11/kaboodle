package commands.mathematical;

import commands.Command;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ModCommandTest {

  @Test
  public void
      givenModCommandWithTwoChildCommands_whenRunningCommand_thenReturnModOfChildCommandsValues() {
    final String firstMockReturnValue = "5";
    final String secondMockReturnValue = "2";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));
    ModCommand modCommand = new ModCommand(childCommands);
    String sum = modCommand.run();

    String expected = "1";
    assertEquals(expected, sum);
  }

  @Test
  public void
      givenModCommandWithMultipleChildCommands_whenRunningCommand_thenReturnModOfChildCommandsValues() {
    final String firstMockReturnValue = "13";
    final String secondMockReturnValue = "5";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    ModCommand modCommand = new ModCommand(childCommands);
    String sum = modCommand.run();

    String expected = "0";
    assertEquals(expected, sum);
  }

  @Test(expected = NumberFormatException.class)
  public void
      givenModCommandWithChildCommandNotReturningStringNumber_whenRunningCommand_thenThrowsRuntimeException() {
    final String firstMockReturnValue = "2";
    final String secondMockReturnValue = "R";
    final String thirdMockReturnValue = "3";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    ModCommand modCommand = new ModCommand(childCommands);
    modCommand.run();
  }
}
