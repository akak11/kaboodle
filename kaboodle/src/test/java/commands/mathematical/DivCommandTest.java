package commands.mathematical;

import commands.Command;
import org.junit.Test;
import testingUtils.MockBuilder;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class DivCommandTest {

  @Test
  public void givenDivCommandWithTwoChildCommands_whenRunningCommand_thenReturnDividedValue() {
    final String firstMockReturnValue = "10";
    final String secondMockReturnValue = "5";

    List<Command> childCommands =
        MockBuilder.getMockList(Arrays.asList(firstMockReturnValue, secondMockReturnValue));

    DivCommand divCommand = new DivCommand(childCommands);
    String divResult = divCommand.run();

    final String expected = "2";
    assertEquals(expected, divResult);
  }

  @Test
  public void givenDivCommandWithMultipleChildCommands_whenRunningCommand_thenReturnDividedValue() {
    final String firstMockReturnValue = "10";
    final String secondMockReturnValue = "5";
    final String thirdMockReturnValue = "2";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));

    DivCommand divCommand = new DivCommand(childCommands);
    String divResult = divCommand.run();

    final String expected = "1";
    assertEquals(expected, divResult);
  }

  @Test(expected = NumberFormatException.class)
  public void
      givenDivCommandWithChildCommandNotReturningStringNumber_whenRunningCommand_thenThrowRuntimeException() {
    final String firstMockReturnValue = "10";
    final String secondMockReturnValue = "R";
    final String thirdMockReturnValue = "2";

    List<Command> childCommands =
        MockBuilder.getMockList(
            Arrays.asList(firstMockReturnValue, secondMockReturnValue, thirdMockReturnValue));
    DivCommand divCommand = new DivCommand(childCommands);
    divCommand.run();
  }
}
