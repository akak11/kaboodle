package testingUtils;

import commands.Command;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class MockBuilder {

    public static List<Command> getMockList(List<String> mockReturnValues) {

        List<Command> mocks = new ArrayList<>();
        for (String returnValue : mockReturnValues) {
            mocks.add(getMock(returnValue));
        }

        return mocks;
    }

    public static Command getMock(String mockReturnValue) {
        Command command = Mockito.mock(Command.class);
        when(command.run()).thenReturn(mockReturnValue);

        return command;
    }
}
