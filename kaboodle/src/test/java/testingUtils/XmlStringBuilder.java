package testingUtils;

import commands.CommandType;

import java.util.Map;
import java.util.stream.Collectors;

public class XmlStringBuilder {

    public String comment(String message) {
        return "<!-- " + message + " -->";
    }

    public String openTag(CommandType command) {
        return "<" + command.getCommandName() + ">";
    }

    public String closingTag(CommandType command) {
        return "</" + command.getCommandName() + ">";
    }

    public String tag(CommandType command) {
        return "<" + command.getCommandName() + "/>";
    }

    public String tagWithText(CommandType command, String value) {
        return "<" + command.getCommandName() + ">" + value + "</" + command.getCommandName() + ">";
    }

    public String openTagWithAttributes(CommandType command, Map<String, String> attributeNameToValue) {
        String attributes = createAttributesString(attributeNameToValue);
        return "<" + command.getCommandName() + " " + attributes + ">";
    }

    private String createAttributesString(Map<String, String> attributeNameToValue) {
        return attributeNameToValue.keySet().stream()
                .map(key -> key + "='" + attributeNameToValue.get(key) + "'")
                .collect(Collectors.joining(" "));
    }
}
