package parser;

import commands.Command;
import commands.controlStructure.BlockCommand;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import testingUtils.MockBuilder;

import java.util.List;

import static org.mockito.Mockito.verify;

public class CommandsExecutorTest {

    private final CommandsExecutor commandsExecutor = new CommandsExecutor();

    @Test
    public void givenRootCommandWithOneChildCommand_whenExecuting_thenVerifyExecutionOfCommand() {
        String mockReturnValue = "hi";
        Command commandMock = MockBuilder.getMock(mockReturnValue);

        commandsExecutor.execute(commandMock);

        verify(commandMock).run();
    }

    @Test
    public void givenRootCommandWithMultipleChildCommand_whenExecuting_thenVerifyExecutionOfChildCommands() {
        List<Command> commandMocks = MockBuilder.getMockList(List.of("hi", "hello", "bye"));
        BlockCommand rootBlockCommand = new BlockCommand(commandMocks);
        InOrder inOrder =
                Mockito.inOrder(commandMocks.get(0), commandMocks.get(1), commandMocks.get(2));

        commandsExecutor.execute(rootBlockCommand);

        for (Command commandMock : commandMocks) {
            inOrder.verify(commandMock).run();
        }
    }
}
