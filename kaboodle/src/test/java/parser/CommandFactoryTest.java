package parser;

import commands.Command;
import commands.mathematical.AddCommand;
import exceptions.NoSuchCommand;
import org.junit.Test;
import parser.commandFactory.CommandFactory;

import static org.junit.Assert.assertEquals;

public class CommandFactoryTest {

    private final CommandFactory commandFactory = new CommandFactory();

    @Test(expected = NoSuchCommand.class)
    public void givenStringWithIllegalKaboodleCommand_whenGettingCommand_thenThrowException()
            throws NoSuchCommand {
        String commandName = "dd";

        this.commandFactory.getCommand(commandName);
    }

    @Test()
    public void givenStringWithLegalKaboodleCommand_whenGettingCommand_thenGetRelevantCommand()
            throws NoSuchCommand {
        String commandName = "add";

        Command actual = this.commandFactory.getCommand(commandName);
        Command expected = new AddCommand();

        assertEquals(expected, actual);
    }
}
