package parser;

import commands.CommandType;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import testingUtils.XmlStringBuilder;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DOMCreatorTest {

    public static final String VALUE = "hello";
    private final XmlStringBuilder xml = new XmlStringBuilder();
    private final DOMCreator creator = new DOMCreator();

    @Test
    public void givenCodeWithWhitespace_whenCreatingDOM_thenCodeDoesntIncludeThemAsTextNodes() throws Exception {
        final String code = "\r" + xml.tagWithText(CommandType.LITERAL, VALUE)
                + "\t" + xml.tag(CommandType.NOP) + "\n";

        Document result = creator.createFromString(code);
        Node firstChild = result.getFirstChild();

        int expectedAmountOfChildNodes = 2;
        assertEquals(expectedAmountOfChildNodes, firstChild.getChildNodes().getLength());
        assertNotEquals(Node.TEXT_NODE, firstChild.getChildNodes().item(0).getNodeType());
        assertNotEquals(Node.TEXT_NODE, firstChild.getChildNodes().item(1).getNodeType());
    }

    @Test
    public void givenEmptyCode_whenCreatingDOM_thenReturnDocumentWithRootNodeOnly() throws Exception {
        String xmlString = "";
        Document result = creator.createFromString(xmlString);

        int expectedAmountOfChildNodes = 1;
        assertEquals(expectedAmountOfChildNodes, result.getChildNodes().getLength());
        assertEquals(CommandType.BLOCK.getCommandName(), result.getFirstChild().getNodeName());
    }

    @Test
    public void givenCodeWithOnlyOneLevelCommands_whenCreatingDOM_thenReturnDocumentWithRootNodeAndHisChildNodes() throws Exception {
        String xmlString = xml.tag(CommandType.INPUT)
                + xml.tag(CommandType.NOP)
                + xml.tag(CommandType.INPUT);
        Document result = creator.createFromString(xmlString);
        Node resultRootNode = result.getFirstChild();

        int expectedAmountOfChildNodes = 3;
        assertEquals(expectedAmountOfChildNodes, resultRootNode.getChildNodes().getLength());
    }

    @Test
    public void givenCodeWithMoreThanOneLevelCommands_whenCreatingDOM_thenReturnDocumentWithRootNodeAndHisChildNodes() throws Exception {
        String xmlString = xml.tagWithText(CommandType.LITERAL, VALUE)
                + xml.tag(CommandType.NOP)
                + xml.openTag(CommandType.PRINT)
                + xml.tagWithText(CommandType.LITERAL, "hi")
                + xml.closingTag(CommandType.PRINT);
        int printNodeIndex = 2;

        Document result = creator.createFromString(xmlString);
        Node resultRootNode = result.getFirstChild();
        Node childNodeWithChildNodes = resultRootNode.getChildNodes().item(printNodeIndex);

        int expectedAmountOfRootChildNodes = 3;
        int expectedAmountOfPrintChildNodes = 1;
        assertEquals(expectedAmountOfRootChildNodes, resultRootNode.getChildNodes().getLength());
        assertEquals(expectedAmountOfPrintChildNodes, childNodeWithChildNodes.getChildNodes().getLength());
    }

    @Test
    public void givenCodeWithCommandWithText_whenCreatingDOM_thenReturnDocumentWithTextNodeForNodeCommand() throws Exception {
        String xmlString = xml.tagWithText(CommandType.LITERAL, VALUE);

        Document result = creator.createFromString(xmlString);
        Node resultRootNode = result.getFirstChild();
        Node NodeWithTextNode = resultRootNode.getFirstChild();
        Node textNode = NodeWithTextNode.getFirstChild();

        assertEquals(Node.TEXT_NODE, textNode.getNodeType());
    }

    @Test
    public void givenCodeWithCommandWithAttribute_whenCreatingDOM_thenReturnDocumentWithAttributesForNode() throws Exception {
        String xmlString = xml.openTagWithAttributes(CommandType.RANDOM, Map.of("max", "6"))
                + xml.closingTag(CommandType.RANDOM);

        Document result = creator.createFromString(xmlString);
        Node resultRootNode = result.getFirstChild();
        Node NodeWithAttributes = resultRootNode.getFirstChild();
        NamedNodeMap attributes = NodeWithAttributes.getAttributes();

        int expectedAmountOfAttributes = 1;
        assertEquals(expectedAmountOfAttributes, attributes.getLength());
    }
}
