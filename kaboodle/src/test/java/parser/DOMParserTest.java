package parser;

import commands.Command;
import commands.CommandType;
import commands.basic.*;
import commands.controlStructure.BlockCommand;
import commands.logical.NotCommand;
import exceptions.*;
import org.junit.Test;
import org.mockito.Mockito;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import parser.commandFactory.CommandFactory;
import testingUtils.MockBuilder;
import testingUtils.XmlStringBuilder;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class DOMParserTest {

    public static final String MESSAGE = "my comment";
    private final CommandFactory commandFactoryMock = Mockito.mock(CommandFactory.class);
    private final DOMParser domParser = new DOMParser(commandFactoryMock);
    private final DOMCreator domCreator = new DOMCreator();
    private final XmlStringBuilder xml = new XmlStringBuilder();

    @Test
    public void givenDOMWithOneNoContentCommand_whenParsingDOM_thenReturnRootCommandWithOneChildCommand()
            throws Exception {
        String kaboodleCode = xml.tag(CommandType.NOP);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.NOP.getCommandName())).thenReturn(new NopCommand());

        Command expected = new BlockCommand(List.of(new NopCommand()));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test
    public void givenDOMWithOneTextContentCommand_whenParsingDOM_thenReturnRootCommandWithOneChildCommandWithText()
            throws Exception {
        String value = "hello";
        String kaboodleCode = xml.tagWithText(CommandType.LITERAL, value);
        Document codeDOM = domCreator.createFromString(kaboodleCode);
        LiteralCommand literalCommand = new LiteralCommand(value);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());

        Command expected = new BlockCommand(List.of(literalCommand));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test(expected = NoSuchAttributeForCommand.class)
    public void givenStringWithIllegalKaboodleAttribute_whenParsingString_thenThrowException()
            throws Exception {
        String kaboodleCode = xml.openTagWithAttributes(CommandType.TOHEX, Map.of("yay", "5"))
                + xml.tagWithText(CommandType.LITERAL, "756")
                + xml.closingTag(CommandType.TOHEX);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.TOHEX.getCommandName())).thenReturn(new ToHexCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());

        domParser.createCommandsFromDom(codeDOM);
    }

    @Test()
    public void
    givenStringWithOneKaboodleAttribute_whenParsingString_thenRootCommandWithChildCommandContainingAttribute()
            throws Exception {
        int max = 5;
        String kaboodleCode = xml.openTagWithAttributes(CommandType.RANDOM, Map.of(RandomCommand.MAX_ATTRIBUTE_NAME, String.valueOf(max)))
                + xml.closingTag(CommandType.RANDOM);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.RANDOM.getCommandName())).thenReturn(new RandomCommand());

        Command expected = new BlockCommand(List.of(new RandomCommand(new Random(), max)));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test()
    public void
    givenStringWithOneKaboodleCommandWithOneLevelChildCommands_whenParsingString_thenReturnRootCommandWithRelevantChildCommands()
            throws Exception {
        String firstValue = "hi";
        String secondValue = "there";
        String separator = "5";
        String kaboodleCode = xml.openTagWithAttributes(CommandType.CAT, Map.of("separator", separator))
                + xml.tagWithText(CommandType.LITERAL, firstValue)
                + xml.tagWithText(CommandType.LITERAL, secondValue)
                + xml.closingTag(CommandType.CAT);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.CAT.getCommandName())).thenReturn(new CatCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName()))
                .thenReturn(new LiteralCommand(), new LiteralCommand());

        List<Command> childCommands =
                Arrays.asList(new LiteralCommand(firstValue), new LiteralCommand(secondValue));
        Command expected = new BlockCommand(List.of(new CatCommand(separator, childCommands)));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test()
    public void
    givenStringWithOneKaboodleCommandWithTwoLevelChildCommands_whenParsingString_thenReturnRootCommandWithRelevantChildCommands()
            throws Exception {
        String firstValue = "hi";
        String secondValue = "there";
        String thirdValue = "756";
        String separator = "5";
        String kaboodleCode = xml.openTagWithAttributes(CommandType.CAT, Map.of("separator", separator))
                + xml.tagWithText(CommandType.LITERAL, firstValue)
                + xml.tagWithText(CommandType.LITERAL, secondValue)
                + xml.openTag(CommandType.TOHEX)
                + xml.tagWithText(CommandType.LITERAL, thirdValue)
                + xml.closingTag(CommandType.TOHEX)
                + xml.closingTag(CommandType.CAT);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.CAT.getCommandName())).thenReturn(new CatCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName()))
                .thenReturn(new LiteralCommand(), new LiteralCommand(), new LiteralCommand());
        when(commandFactoryMock.getCommand(CommandType.TOHEX.getCommandName())).thenReturn(new ToHexCommand());

        List<Command> childCommands =
                Arrays.asList(
                        new LiteralCommand(firstValue),
                        new LiteralCommand(secondValue),
                        new ToHexCommand(new LiteralCommand(thirdValue)));
        Command expected = new BlockCommand(List.of(new CatCommand(separator, childCommands)));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test()
    public void givenStringWithMultipleKaboodleCommands_whenParsingString_thenReturnRootCommandWithChildCommands()
            throws Exception {
        String firstValue = "hi";
        String secondValue = "there";
        String thirdValue = "756";
        String fourthValue = "hello";
        String separator = "5";
        String kaboodleCode = xml.openTagWithAttributes(CommandType.CAT, Map.of(CatCommand.SEPARATOR_ATTRIBUTE_NAME, separator))
                + xml.tagWithText(CommandType.LITERAL, firstValue)
                + xml.tagWithText(CommandType.LITERAL, secondValue)
                + xml.openTag(CommandType.TOHEX)
                + xml.tagWithText(CommandType.LITERAL, thirdValue)
                + xml.closingTag(CommandType.TOHEX)
                + xml.closingTag(CommandType.CAT)
                + xml.tagWithText(CommandType.LITERAL, fourthValue);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.CAT.getCommandName())).thenReturn(new CatCommand());
        when(commandFactoryMock.getCommand(CommandType.TOHEX.getCommandName())).thenReturn(new ToHexCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName()))
                .thenReturn(
                        new LiteralCommand(), new LiteralCommand(), new LiteralCommand(), new LiteralCommand());

        List<Command> childCommands =
                Arrays.asList(
                        new LiteralCommand(firstValue),
                        new LiteralCommand(secondValue),
                        new ToHexCommand(new LiteralCommand(thirdValue)));
        Command expected = new BlockCommand(List.of(new CatCommand(separator, childCommands), new LiteralCommand(fourthValue)));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void
    givenStringWithKaboodleCommandsAndTooLittleChildCommandsAmount_whenParsingString_thenThrowException()
            throws Exception {
        String kaboodleCode = xml.openTagWithAttributes(CommandType.CAT, Map.of("separator", "5"))
                + xml.tagWithText(CommandType.LITERAL, "hi")
                + xml.closingTag(CommandType.CAT);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.CAT.getCommandName())).thenReturn(new CatCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());

        domParser.createCommandsFromDom(codeDOM);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void
    givenStringWithKaboodleCommandsAndTooMuchChildCommandsAmount_whenParsingString_thenThrowException()
            throws Exception {
        String value = "756";
        String kaboodleCode = xml.openTag(CommandType.TOHEX)
                + xml.tagWithText(CommandType.LITERAL, value)
                + xml.tagWithText(CommandType.LITERAL, value)
                + xml.closingTag(CommandType.TOHEX);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());
        when(commandFactoryMock.getCommand(CommandType.TOHEX.getCommandName())).thenReturn(new ToHexCommand());

        domParser.createCommandsFromDom(codeDOM);
    }

    @Test(expected = MissingAttribute.class)
    public void
    givenStringWithKaboodleCommandsAndMissingAttribute_whenParsingString_thenThrowException()
            throws Exception {
        String kaboodleCode = xml.tag(CommandType.RANDOM);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());
        when(commandFactoryMock.getCommand(CommandType.RANDOM.getCommandName())).thenReturn(new RandomCommand());

        domParser.createCommandsFromDom(codeDOM);
    }

    @Test(expected = IllegalNumberOfChildCommands.class)
    public void
    givenStringWithKaboodleCommandThatShouldHaveZeroChildCommandsButHasThem_whenParsingString_thenThrowException()
            throws Exception {
        String kaboodleCode = xml.openTag(CommandType.LITERAL)
                + xml.tagWithText(CommandType.LITERAL, "there")
                + xml.tagWithText(CommandType.LITERAL, "756")
                + xml.tagWithText(CommandType.LITERAL, "hello")
                + xml.closingTag(CommandType.LITERAL);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand());

        domParser.createCommandsFromDom(codeDOM);
    }

    @Test
    public void givenStringWithKaboodleCodeContainingCommentNodesOnFirstLevel_whenParsingString_thenReturnRootCommandWithoutComments() throws Exception {
        String firstValue = "there";
        String secondValue = "756";
        String thirdValue = "hello";
        String kaboodleCode = xml.comment(MESSAGE)
                + xml.tagWithText(CommandType.LITERAL, firstValue)
                + xml.tagWithText(CommandType.LITERAL, secondValue)
                + xml.comment(MESSAGE)
                + xml.tagWithText(CommandType.LITERAL, thirdValue);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName())).thenReturn(new LiteralCommand(), new LiteralCommand(), new LiteralCommand());


        Command parsedCommand = domParser.createCommandsFromDom(codeDOM);
        Command expected = new BlockCommand(List.of(new LiteralCommand(firstValue), new LiteralCommand(secondValue), new LiteralCommand(thirdValue)));
        assertEquals(expected, parsedCommand);
    }

    @Test
    public void givenStringWithKaboodleCodeContainingCommentNodesNotOnFirstLevel_whenParsingString_thenReturnRootCommandWithoutComments() throws Exception {
        String firstValue = "hi";
        String secondValue = "there";
        String thirdValue = "756";
        String fourthValue = "hello";
        String separator = "5";
        String kaboodleCode = xml.openTagWithAttributes(CommandType.CAT, Map.of("separator", separator))
                + xml.tagWithText(CommandType.LITERAL, firstValue)
                + xml.tagWithText(CommandType.LITERAL, secondValue)
                + xml.comment(MESSAGE)
                + xml.openTag(CommandType.TOHEX)
                + xml.comment(MESSAGE)
                + xml.tagWithText(CommandType.LITERAL, thirdValue)
                + xml.closingTag(CommandType.TOHEX)
                + xml.closingTag(CommandType.CAT)
                + xml.tagWithText(CommandType.LITERAL, fourthValue);
        Document codeDOM = domCreator.createFromString(kaboodleCode);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(CommandType.CAT.getCommandName())).thenReturn(new CatCommand());
        when(commandFactoryMock.getCommand(CommandType.TOHEX.getCommandName())).thenReturn(new ToHexCommand());
        when(commandFactoryMock.getCommand(CommandType.LITERAL.getCommandName()))
                .thenReturn(
                        new LiteralCommand(), new LiteralCommand(), new LiteralCommand(), new LiteralCommand());

        List<Command> childCommands =
                Arrays.asList(
                        new LiteralCommand(firstValue),
                        new LiteralCommand(secondValue),
                        new ToHexCommand(new LiteralCommand(thirdValue)));
        Command expected = new BlockCommand(List.of(new CatCommand(separator, childCommands), new LiteralCommand(fourthValue)));
        Command actual = domParser.createCommandsFromDom(codeDOM);
        assertEquals(expected, actual);
    }

    @Test
    public void givenCommandWithNoTextPropertyAndNoTextNode_whenUpdatingTextProperty_thenNoTextAdded() throws IllegalTextInCommand {
        Node nodeMock = Mockito.mock(Node.class);
        NodeList childNodesMock = Mockito.mock(NodeList.class);
        when(nodeMock.getChildNodes()).thenReturn(childNodesMock);
        when(childNodesMock.getLength()).thenReturn(0);
        when(nodeMock.hasAttributes()).thenReturn(false);

        Command childCommandMock = MockBuilder.getMock("hello");
        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(childCommandMock);

        Command command = new NotCommand(childCommandMock);
        Command expected = new NotCommand(childCommandMock);

        domParser.createCommand(nodeMock);

        assertEquals(expected, command);
    }

    @Test(expected = IllegalTextInCommand.class)
    public void givenCommandWithNoTextPropertyAndTextNode_whenUpdatingTextProperty_thenThrowException() throws IllegalTextInCommand {
        Node nodeMock = Mockito.mock(Node.class);
        Node childNodeMock = Mockito.mock(Node.class);
        NodeList childNodesMock = Mockito.mock(NodeList.class);

        when(nodeMock.getChildNodes()).thenReturn(childNodesMock);
        when(childNodesMock.getLength()).thenReturn(1);
        when(childNodesMock.item(0)).thenReturn(childNodeMock);
        when(childNodeMock.getNodeType()).thenReturn(Node.TEXT_NODE);
        when(childNodeMock.getNodeValue()).thenReturn("text");

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(new NotCommand());

        domParser.createCommand(nodeMock);
    }

    @Test
    public void givenCommandWithTextPropertyAndTextNode_whenUpdatingTextProperty_thenAddTextPropertyToCommand() throws IllegalTextInCommand {
        Node nodeMock = Mockito.mock(Node.class);
        NodeList childNodesMock = Mockito.mock(NodeList.class);
        when(nodeMock.getChildNodes()).thenReturn(childNodesMock);
        when(childNodesMock.getLength()).thenReturn(1);
        Node childNodeMock = Mockito.mock(Node.class);
        when(childNodesMock.item(0)).thenReturn(childNodeMock);
        String value = "text";
        when(childNodeMock.getNodeValue()).thenReturn(value);
        when(childNodeMock.getNodeType()).thenReturn(Node.TEXT_NODE);

        Command command = new LiteralCommand();
        Command expected = new LiteralCommand(value);

        when(commandFactoryMock.getCommand(CommandType.BLOCK.getCommandName())).thenReturn(new BlockCommand());
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);


        domParser.createCommand(nodeMock);

        assertEquals(expected, command);
    }

    @Test
    public void givenCommandWithNoAttributesAndNodeWithNoAttributes_whenUpdatingAttributes_thenCommandStaysTheSame() {
        Node nodeMock = Mockito.mock(Node.class);
        when(nodeMock.hasAttributes()).thenReturn(false);

        NodeList childNodes = Mockito.mock(NodeList.class);
        when(nodeMock.getChildNodes()).thenReturn(childNodes);
        when(childNodes.getLength()).thenReturn(0);

        Command command = new LiteralCommand();
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);

        domParser.createCommand(nodeMock);

        Command expected = new LiteralCommand();
        assertEquals(expected, command);
    }


    @Test(expected = NoSuchAttributeForCommand.class)
    public void givenNodeWithNoAttributesAndNodeWithAttributes_whenUpdatingAttributes_thenThrowException() throws AttributesException {
        Node nodeMock = Mockito.mock(Node.class);
        when(nodeMock.hasAttributes()).thenReturn(true);

        NamedNodeMap nodeMapMock = Mockito.mock(NamedNodeMap.class);
        when(nodeMock.getAttributes()).thenReturn(nodeMapMock);
        when(nodeMapMock.getLength()).thenReturn(1);

        Node attributeNodeMock = Mockito.mock(Node.class);
        when(nodeMapMock.item(0)).thenReturn(attributeNodeMock);
        when(attributeNodeMock.getNodeValue()).thenReturn("5");
        when(attributeNodeMock.getNodeName()).thenReturn("max");

        Command command = new LiteralCommand();
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);

        domParser.createCommand(nodeMock);
    }

    @Test()
    public void givenNodeWithAttributesAndNodeWithValidAttributes_whenUpdatingAttributes_thenUpdateCommandAttributes() throws AttributesException {
        Node nodeMock = Mockito.mock(Node.class);
        when(nodeMock.hasAttributes()).thenReturn(true);

        NodeList childNodes = Mockito.mock(NodeList.class);
        when(nodeMock.getChildNodes()).thenReturn(childNodes);
        when(childNodes.getLength()).thenReturn(0);

        NamedNodeMap nodeMapMock = Mockito.mock(NamedNodeMap.class);
        when(nodeMock.getAttributes()).thenReturn(nodeMapMock);
        when(nodeMapMock.getLength()).thenReturn(1);

        Node attributeNodeMock = Mockito.mock(Node.class);
        when(nodeMapMock.item(0)).thenReturn(attributeNodeMock);
        when(attributeNodeMock.getNodeValue()).thenReturn("5");
        when(attributeNodeMock.getNodeName()).thenReturn("max");

        Command command = new RandomCommand();
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);

        domParser.createCommand(nodeMock);

        Command expected = new RandomCommand(new Random(), 5);
        assertEquals(expected, command);
    }

    @Test(expected = NoSuchAttributeForCommand.class)
    public void givenNodeWithAttributesAndNodeWithNotValidAttributes_whenUpdatingAttributes_thenUpdateCommandAttributes() throws AttributesException {
        Node nodeMock = Mockito.mock(Node.class);
        when(nodeMock.hasAttributes()).thenReturn(true);

        NamedNodeMap nodeMapMock = Mockito.mock(NamedNodeMap.class);
        when(nodeMock.getAttributes()).thenReturn(nodeMapMock);
        when(nodeMapMock.getLength()).thenReturn(1);

        Node attributeNodeMock = Mockito.mock(Node.class);
        when(nodeMapMock.item(0)).thenReturn(attributeNodeMock);
        when(attributeNodeMock.getNodeValue()).thenReturn("5", "6");
        when(attributeNodeMock.getNodeName()).thenReturn("yay", "max");

        Command command = new RandomCommand();
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);

        domParser.createCommand(nodeMock);
    }

    @Test(expected = MissingAttribute.class)
    public void givenNodeWithAttributesAndNodeWithoutMustHaveAttribute_whenUpdatingAttributes_thenThrowException() throws AttributesException {
        Node nodeMock = Mockito.mock(Node.class);
        when(nodeMock.hasAttributes()).thenReturn(true);

        NamedNodeMap nodeMapMock = Mockito.mock(NamedNodeMap.class);
        when(nodeMock.getAttributes()).thenReturn(nodeMapMock);
        when(nodeMapMock.getLength()).thenReturn(1);

        Node attributeNodeMock = Mockito.mock(Node.class);
        when(nodeMapMock.item(0)).thenReturn(attributeNodeMock);
        when(attributeNodeMock.getNodeValue()).thenReturn("6");
        when(attributeNodeMock.getNodeName()).thenReturn("min");

        Command command = new RandomCommand();
        when(commandFactoryMock.getCommand(nodeMock.getNodeName())).thenReturn(command);

        domParser.createCommand(nodeMock);
    }
}
