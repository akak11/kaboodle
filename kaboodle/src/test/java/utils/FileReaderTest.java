package utils;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class FileReaderTest {

    private final String FILE_NAME = "test.txt";
    private final FileReader fileReader = new FileReader();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void givenEmptyFileName_whenReadingFile_thenReturnEmptyString() throws IOException {
        File file = folder.newFile(FILE_NAME);
        String text = this.fileReader.readFromFileToString(file.getAbsolutePath());

        String expected = "";
        assertEquals(expected, text);
    }

    @Test
    public void givenNotEmptyFileName_whenReadingFile_thenReturnEmptyString() throws IOException {
        String expectedFileText = "this is the file text";
        File file = folder.newFile(FILE_NAME);

        FileWriter myWriter = new FileWriter(file.getAbsolutePath());
        myWriter.write(expectedFileText);
        myWriter.close();

        String text = this.fileReader.readFromFileToString(file.getAbsolutePath());
        assertEquals(expectedFileText, text);
    }
}
